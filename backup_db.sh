#!/usr/bin/env bash
set -e

# Создание директории для временных файлов в директории проекта
mkdir -p $CI_PROJECT_DIR/tmp
LOCAL_TEMP_FILE="$CI_PROJECT_DIR/tmp/$(basename $TEMP_FILE)"

# Логирование будет выполняться в файл внутри директории проекта
LOGFILE="$CI_PROJECT_DIR/tmp/backup.log"

# Если первый день месяца, переименовываем лог-файл
if [ $(date +%d) -eq 1 ]; then
    mv "$LOGFILE" "${LOGFILE}.1"
fi

exec >> "$LOGFILE"
exec 2>&1

. .backuprc

export PGHOST PGPORT PGDATABASE PGUSER PGPASSWORD BUCKET_NAME TEMP_FILE

PGPASSWORD=$PGPASSWORD pg_dump -Fc --no-acl -h $PGHOST -p $PGPORT -U $PGUSER -d $PGDATABASE > "$LOCAL_TEMP_FILE"
if [ $? -ne 0 ]; then
    echo "Ошибка при создании бэкапа"
    exit 1
fi

echo "Бэкап успешно создан: $LOCAL_TEMP_FILE"
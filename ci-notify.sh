#!/usr/bin/bash

JOB="$1"
URL="https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage"

TEXT="
Ostap S. build $JOB

Артефакт:
$CI_PROJECT_URL/-/jobs/artifacts/$CI_COMMIT_SHA/download?job=$JOB
"

curl --location $URL \
    -H "Content-type: application/json" \
    -d "{\"chat_id\": -4135948752, \"text\": \"$TEXT\"}"




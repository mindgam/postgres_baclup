### Создание резервной копии базы данных PostgreSQL и загрузки ее на удаленное хранилище S3

| Функция            | Описание                                                                                                       |
|---------------------|----------------------------------------------------------------------------------------------------------------|
| backup_dt.sh        | Скрипт для создания резервной копии базы данных PostgreSQL и загрузки ее на удаленное хранилище S3.          |
| backup.rc           | Файл конфигурации, содержащий переменные окружения для подключения к базе данных и хранилищу S3.             |

### Выходные данные в виде переменных:

| Переменная  | Описание                                                                                                 |
|-------------|----------------------------------------------------------------------------------------------------------|
| LOGFILE     | Путь к журнальному файлу резервного копирования.                                                          |
| TIMESTAMP   | Временная метка для создания уникального имени файла резервной копии.                                    |
| TEMP_FILE   | Временный файл для сохранения резервной копии базы данных перед загрузкой на S3.                         |
| S3_FILE     | Путь к файлу резервной копии на удаленном хранилище S3 в зависимости от дня недели и месяца.            |
| PGHOST      | Хост базы данных PostgreSQL.                                                                             |
| PGPORT      | Порт базы данных PostgreSQL.                                                                             |
| PGDATABASE  | Название базы данных PostgreSQL.                                                                         |
| PGUSER      | Пользователь базы данных PostgreSQL.                                                                     |
| PGPASSWORD  | Пароль пользователя базы данных PostgreSQL.                                                               |
| BUCKET_NAME | Имя бакета на удаленном хранилище S3 для сохранения резервных копий.                                     |

### 

PGHOST, PGPORT, PGDATABASE, PGUSE, PGPASSWORD, BUCKET_NAME переменные настраиваются в Settings, CI/DC Variables GitlabCI/CD сервера.

###

В скрипте ci-notify.sh добавлено вывод сообщения перед отправкой уведомления в Telegram о завершении работы по резервному копированию. Теперь при выполнении скрипта будет отображаться сообщение "Отправка уведомления в Telegram о завершении сборки $JOB".

####  gitlab-ci `postgres_backup`:

- **Описание**: Задача для создания резервной копии базы данных PostgreSQL и сохранения её во временном файле перед загрузкой на удаленное хранилище S3.
- **Стадия**: `backup`
- **Образ**: `postgres:15`
- **Теги**:
  - `infra-backup-db-docker`


1. Формирование файла конфигурации `.backuprc` из шаблона `.backuprc.template`:
    ```bash
    envsubst < .backuprc.template > .backuprc
    ```

##### Шаги выполнения:

1. Выполнение скрипта `backup_db.sh` с параметром `-x`:
    ```bash
    ./backup_db.sh -x
    ```

##### Артефакты и время хранения:

- **Артефакты**:
  - `tmp/`

- **Срок хранения**:
  - `1 hour`

#### `postgres_deploy`:

- **Описание**: Задача для загрузки резервной копии базы данных на удаленное хранилище S3 и управления предыдущими резервными копиями.
- **Стадия**: `deploy`
- **Образ**: `postgres:15`
- **Теги**:
  - `infra-backup-db-docker`

##### Предварительные шаги:

1. Установка необходимых пакетов:
    ```bash
    apt-get update && apt-get install -y s3cmd
    ```

2. Конфигурирование доступа к удаленному хранилищу S3:
    ```bash
    echo "[default]" > ~/.s3cfg
    echo "access_key = $ACCESS_KEY" >> ~/.s3cfg
    echo "secret_key = $SECRET_KEY" >> ~/.s3cfg
    echo "host_base = storage.yandexcloud.net" >> ~/.s3cfg
    echo "host_bucket = %(bucket)s.storage.yandexcloud.net" >> ~/.s3cfg
    echo "bucket_location = ru-central1" >> ~/.s3cfg
    ```

##### Шаги выполнения:

1. Формирование пути к файлу на S3 в зависимости от дня недели и месяца:
    ```bash
    TIMESTAMP=$(date +%F_%T | tr ':' '-')
    if [ $(date +%w) -eq 0 ]; then
        S3_FILE="s3://$BUCKET_NAME/$PGDATABASE.weekly-backup-$TIMESTAMP"
    elif [ $(date +%d) -eq 1 ]; then
        S3_FILE="s3://$BUCKET_NAME/$PGDATABASE.monthly-backup-$TIMESTAMP"
    else
        S3_FILE="s3://$BUCKET_NAME/$PGDATABASE.backup-$TIMESTAMP"
    fi
    ```

2. Загрузка локального файла на S3 и управление предыдущими резервными копиями:
    ```bash
    s3cmd put $CI_PROJECT_DIR/tmp/$(basename $TEMP_FILE) $S3_FILE || { echo "Failed to upload $TEMP_FILE to $S3_FILE"; exit 1; }
    s3cmd ls "s3://$BUCKET_NAME/backup-*" | awk '{ print $4 }' | sort -r | sed '1,6 d' | xargs -r -L1 s3cmd rm
    s3cmd ls "s3://$BUCKET_NAME/weekly-backup-*" | awk '{ print $4 }' | sort -r | sed '1,6 d' | xargs -r -L1 s3cmd rm
    s3cmd ls "s3://$BUCKET_NAME/monthly-backup-*" | awk '{ print $4 }' | sort -r | sed '1,6 d' | xargs -r -L1 s3cmd rm
    s3cmd ls s3://$BUCKET_NAME
    ```

##### Зависимости:

- `postgres_backup`.
